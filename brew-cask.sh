#!/bin/bash


# to maintain cask ....
#     brew update && brew upgrade brew-cask && brew cleanup && brew cask cleanup`


# Install native apps

brew install caskroom/cask/brew-cask
brew tap caskroom/versions

# -- manual install required
# brew cask install snagit 
# brew cask install 1password

# daily
brew cask install spectacle
brew cask install dropbox
brew cask install rescuetime
brew cask install flux
brew cask install cleanmymac
brew cask install mailplane
brew cask install little-snitch
brew cask install micro-snitch
brew cask install slack
brew cask install screenhero
brew cask install radiant-player

# dev
brew cask install iterm2-beta
brew cask install sublime-text
brew cask install imagealpha
brew cask install imageoptim
brew cask install wireshark
brew cask install gitkraken
brew cask install sourcetree
brew cask install postgres
brew cask install datagrip

# docs/files
brew cask install evernote
brew cask install google-drive

# fun
brew cask install limechat
brew cask install miro-video-converter
brew cask install horndis               # usb tethering

# browsers
brew cask install google-chrome
brew cask install google-chrome-canary
brew cask install firefox
brew cask install chromium
brew cask install torbrowser

# less often
brew cask install disk-inventory-x
brew cask install screenflow
brew cask install vlc
brew cask install gpgtools
brew cask install utorrent
brew cask install deluge

# Not on cask but I want regardless.

# File Multi Tool 5
# Phosphor
